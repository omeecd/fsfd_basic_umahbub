#!/usr/bin/env python
"""
==============================================================
Face detector: uses cascades of partial face detectors and
	       combines the results using graph minimization.

@author: Upal Mahbub, University of Maryland, College Park
	 umahbub@umd.edu
==============================================================

"""


print(__doc__)

from time import time

import numpy as np
import cv2
import cv2.cv as cv
import sys
import os
import glob
import scipy as sc
from scipy import ndimage
from string import maketrans
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
#import pywt
import operator
#from nms_Upal import non_max_suppression_Upal
from Face_clustering_UpalWithFiducials_Test_SVM import Face_Cluster
from PIL import Image, ImageDraw, ImageEnhance, ImageFilter
import argparse
import pdb
from multiprocessing import Pool, Process, Manager
import multiprocessing
import pickle
import itertools
from sklearn import svm


face_cascade_frontal = cv2.CascadeClassifier('./xmlmodels/haarcascade_frontalface_alt2.xml')
face_cascade_profile = cv2.CascadeClassifier('haarcascade_profileface.xml')

face_cascade_eyepair = cv2.CascadeClassifier('./xmlmodels/classifier_EP/haarcascade_mcs_eyepair_big.xml')
face_cascade_nose = cv2.CascadeClassifier('./xmlmodels/classifier_NS/haarcascade_mcs_nose.xml')

face_cascade_U_3_4 = cv2.CascadeClassifier('./xmlmodels/classifier_U_3_4LBP/cascade.xml')
face_cascade_UR_3_4 = cv2.CascadeClassifier('./xmlmodels/classifier_UR_3_4LBP/cascade.xml')
face_cascade_UL_3_4 = cv2.CascadeClassifier('./xmlmodels/classifier_UR_3_4LBP/cascade.xml')
face_cascade_L_3_4 = cv2.CascadeClassifier('./xmlmodels/classifier_R_3_4LBP/cascade.xml')
face_cascade_R_3_4 = cv2.CascadeClassifier('./xmlmodels/classifier_R_3_4LBP/cascade.xml')
face_cascade_B_3_4 = cv2.CascadeClassifier('./xmlmodels/classifier_B_3_4LBP/cascade.xml')
face_cascade_U_1_2 = cv2.CascadeClassifier('./xmlmodels/classifier_U_1_2LBP/cascade.xml')
face_cascade_B_1_2 = cv2.CascadeClassifier('./xmlmodels/classifier_B_1_2LBP/cascade.xml')
face_cascade_R_1_2 = cv2.CascadeClassifier('./xmlmodels/classifier_R_1_2LBP/cascade.xml')
face_cascade_UR_1_2 = cv2.CascadeClassifier('./xmlmodels/classifier_UR_1_2LBP/cascade.xml')
face_cascade_L_1_2 = cv2.CascadeClassifier('./xmlmodels/classifier_R_1_2LBP/cascade.xml')
face_cascade_UL_1_2 = cv2.CascadeClassifier('./xmlmodels/classifier_UR_1_2LBP/cascade.xml')

########### Parse Arguments ###################################################
parser=argparse.ArgumentParser()
parser.add_argument("-Comb","--Combination", help="Combinationo of parts", default = '0')
parser.add_argument("-plt", "--plot", help="Turn on plot", action="store_true", default="false")
parser.add_argument("-PP", "--PreProc", help="Enable Image Preprocessing", action="store_true", default="false")
parser.add_argument("-ImgSv", "--ImgSave", help="Save cropped and proposed images", action="store_true", default="false")
parser.add_argument("-fFlag", "--fiduFlag", help="Find fiducials (eye location)", action="store_true", default="false")
parser.add_argument("-p", "--pool", help="Turn on pooling", action="store_true", default = 'false')
parser.add_argument("-pNo","--processNo", help="Number of parallel processes", type=int, default = 5)
parser.add_argument("-Sc","--ScaleFact", help="Scale Factor (the image will be downsampled by this factor)", type=int, default = 4)
parser.add_argument("-Th","--ThreshN", help="Olap threshold trained for", type=int, default = 60)
parser.add_argument("-stype", "--ScoreType", help="Type of Score", default='ValueSum', choices=['ValueSum', 'ValueProd'])
parser.add_argument("-Tn","--TrSetNo", help="Training Set Number (1, 2, 3, ...)", type=int, default = 1)
parser.add_argument("-Tr","--TrainRatio", help="Percent train data (between 1 to 100)", type=int, default = 20)
parser.add_argument("-INTe","--TeImgNo", help="Test Selected Set Number", type=int, default = 100)
parser.add_argument("-InFileList", "--ListOfTestFiles", help="Text file containing path to the test images", default='./input/AANotDetected.txt')
parser.add_argument("-RotateVal","--RotationInAngle", help="Angle of rotation before processing", type=int, default = -90)
parser.add_argument("-resLoc", "--resFile", help="Location and name of the result file", default='./result/TestImageOut.txt')

args=parser.parse_args()

plot = args.plot
PreProc= args.PreProc
combNo = args.Combination
ImgSave = args.ImgSave
pooling = args.pool
processNo=args.processNo
ThreshN=args.ThreshN

INTe=args.TeImgNo
scaleFact = args.ScaleFact	# Image is downsampled by this factor
ScoreType = args.ScoreType
resizeFact = 1.0/scaleFact    	# resizeFact = 1/scaleFact

n=args.TrSetNo
TrainRatio=args.TrainRatio

InFileList=args.ListOfTestFiles
rotateVal = args.RotationInAngle
OutFileName=args.resFile
########### Parameters ###################################################
if PreProc==1:
	SVMFile = './models_PP_LB/SVM_TrainFinal'+str(n)+"_"+str(TrainRatio)+'_Proposed_umd_PFD_Comb'+str(combNo)+'_Scale'+str(scaleFact)+'_IN'+str(INTe)+'_PP'+'_'+str(ThreshN)+'percent.dat'
	ImpFile = './models_PP_LB/FinalImp_TrainFinal'+str(n)+"_"+str(TrainRatio)+'_Proposed_umd_PFD_Comb'+str(combNo)+'_Scale'+str(scaleFact)+'_IN'+str(INTe)+'_PP'+'_'+str(ThreshN)+'percent.pkl'
	OutFolder = "./result/"
else:
	SVMFile = './models_LB/SVM_TrainFinal'+str(n)+"_"+str(TrainRatio)+'_Proposed_umd_PFD_Comb'+str(combNo)+'_Scale'+str(scaleFact)+'_IN'+str(INTe)+'_'+str(ThreshN)+'percent.dat'
	ImpFile = './models_LB/FinalImp_TrainFinal'+str(n)+"_"+str(TrainRatio)+'_Proposed_umd_PFD_Comb'+str(combNo)+'_Scale'+str(scaleFact)+'_IN'+str(INTe)+'_'+str(ThreshN)+'percent.pkl'
	OutFolder = "./result/"

Comb={}
Comb['0']={}
Comb['5']={}

Comb['0']['Parts']={}
Comb['5']['Parts']={}

Comb['0']['Thresh']= {}
Comb['5']['Thresh']= {}


Comb['0']['Parts']= {'Nose':1, 'EyePar':1, 'UL34':1, 'UR34':1, 'U12':1, 'L34':1, 'UL12':1, 'R12':1, 'L12':1, 'U34':1, 'UR12':1,  'B34':1, 'R34':1, 'B12':1, 'VJ':0}
Comb['5']['Parts']= {'Nose':1, 'EyePar':1, 'UL34':1, 'UR34':1, 'U12':1, 'L34':1, 'UL12':1, 'R12':1, 'L12':1, 'U34':0, 'UR12':0,  'B34':0, 'R34':0, 'B12':0, 'VJ':0}


Comb['0']['Thresh']= {'Dist':6, 'olap': 2, 'imp': 5}
Comb['5']['Thresh']= {'Dist':6, 'olap': 2, 'imp': 5}

PTag={'U12':2,'Nose':5, 'UR34':8, 'EyePar':13, 'U34':4, 'UR12':12, 'UL34':7, 'R34':16, 'R12':10,  'UL12':11, 'L34':15, 'B34':17, 'L12':9, 'B12':6 ,'VJ':1}
PNeigh={'U12':3,'Nose':3, 'UR34':3, 'EyePar':3, 'U34':8, 'UR12':3, 'UL34': 3, 'R34':3, 'R12':3,  'UL12':3, 'L34':3, 'B34':3, 'L12':3, 'B12': 3,'VJ': 1}


FlagDict=Comb[combNo]['Parts']
distThresh = Comb[combNo]['Thresh']['Dist']
olapThresh = Comb[combNo]['Thresh']['olap']
impThresh = Comb[combNo]['Thresh']['imp']
fiduFlag = args.fiduFlag


#############################################################################################


#from blurFunc, 
def facedetect_Upal(img, windowName, face_cascade, flag, color, thresh, lev, msize):
  #print img.shape
  '''
  This function calls VJ Face Detector from openCV and retruns the resulting bounding boxes
  '''
  global plot
  gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
  faces = face_cascade.detectMultiScale(gray, scaleFactor= thresh, minNeighbors=lev, minSize=msize)  
  if (flag==0):
      flag =1;
  boundingBox=np.empty([len(faces), 4], dtype = int)
  i=0
  for (x,  y, w, h) in (faces):
      boundingBox[i:]= (x, y, x+w, y+h)
      i=i+1
  newImg= img.copy()
  if plot ==1:  
          for (x,  y, w, h) in (faces):
              cv2.rectangle(newImg, (x,y), (x+w, y+h), color, 2)
	  cv2.namedWindow(windowName, cv2.WINDOW_NORMAL)
  	  #cv2.resizeWindow(windowName, img.shape[1]/2, img.shape[0]/2)
          cv2.imshow(windowName, newImg);
	  #cv2.waitKey()
  return flag, boundingBox


def blur_edge(img, d=31):
    h, w = img.shape[:2]
    img_pad = cv2.copyMakeBorder(img, d, d, d, d, cv2.BORDER_WRAP)
    img_blur = cv2.GaussianBlur(img_pad, (2*d+1, 2*d+1), -1)[d:-d,d:-d]
    y, x = np.indices((h, w))
    dist = np.dstack([x, w-x-1, y, h-y-1]).min(-1)
    w = np.minimum(np.float32(dist)/d, 1.0)
    return img*w + img_blur*(1-w)

def motion_kernel(angle, d, sz=65):
    kern = np.ones((1, d), np.float32)
    c, s = np.cos(angle), np.sin(angle)
    A = np.float32([[c, -s, 0], [s, c, 0]])
    sz2 = sz // 2
    A[:,2] = (sz2, sz2) - np.dot(A[:,:2], ((d-1)*0.5, 0))
    kern = cv2.warpAffine(kern, A, (sz, sz), flags=cv2.INTER_CUBIC)
    return kern

def defocus_kernel(d, sz=65):
    kern = np.zeros((sz, sz), np.uint8)
    cv2.circle(kern, (sz, sz), d, 255, -1, cv2.CV_AA, shift=1)
    kern = np.float32(kern) / 255.0
    return kern

def deblur_proc(img):
    filter_blurred_l = ndimage.gaussian_filter(img, 0.5)
    alpha = 1
    sharpened = img + alpha * (img - filter_blurred_l)
    return sharpened
    
def median_filt(img, size):
    med_denoised = img
    med_denoised = ndimage.median_filter(img, size)
    return med_denoised

def is_blur(im):
    thresh = 35
    MinZero = 0.05
    
    x=np.asarray(im)
    x_cropped=x[0:(np.shape(x)[0]/16)*16 - 1,0:(np.shape(x)[1]/16)*16 - 1]
    LL1,(LH1,HL1,HH1)=pywt.dwt2(x_cropped,'haar')
    LL2,(LH2,HL2,HH2)=pywt.dwt2(LL1 ,'haar')
    LL3,(LH3,HL3,HH3)=pywt.dwt2(LL2 ,'haar')
    Emap1=np.square(LH1) + np.square(HL1) + np.square(HH1)
    Emap2=np.square(LH2) + np.square(HL2) + np.square(HH2)
    Emap3=np.square(LH3) + np.square(HL3) + np.square(HH3)
     
    dimx=np.shape(Emap1)[0]/8
    dimy=np.shape(Emap1)[1]/8
    Emax1=[]
    vert=1
    for j in range(0,dimx - 2):
        horz=1;
        Emax1.append([])
        for k in range(0,dimy - 2):
            Emax1[j].append(np.max(np.max(Emap1[vert:vert+7,horz:horz+7])))
            horz=horz+8
        vert=vert+8
     
    dimx=np.shape(Emap2)[0]/4
    dimy=np.shape(Emap2)[1]/4
    Emax2=[]
    vert=1
    for j in range(0,dimx - 2):
        horz=1;
        Emax2.append([])
        for k in range(0,dimy - 2):
            Emax2[j].append(np.max(np.max(Emap2[vert:vert+3,horz:horz+3])))
            horz=horz+4
        vert=vert+4
     
    dimx=np.shape(Emap3)[0]/2
    dimy=np.shape(Emap3)[1]/2
    Emax3=[]
    vert=1
    for j in range(0,dimx - 2):
        horz=1;
        Emax3.append([])
        for k in range(0,dimy - 2):
            Emax3[j].append(np.max(np.max(Emap3[vert:vert+1,horz:horz+1])))
            horz=horz+2
        vert=vert+2
     
    N_edge=0
    N_da=0
    N_rg=0
    N_brg=0
     
    EdgeMap = []
    for j in range(0, dimx - 2):
        EdgeMap.append([])
        for k in range(0, dimy - 2):
            if (Emax1[j][k]>thresh) or (Emax2[j][k]>thresh) or (Emax3[j][k]>thresh):
                EdgeMap[j].append(1)
                N_edge = N_edge + 1
                rg = 0
                if (Emax1[j][k]>Emax2[j][k]) and (Emax2[j][k]>Emax3[j][k]):
                    N_da=N_da+1
                elif (Emax1[j][k]<Emax2[j][k]) and (Emax2[j][k]<Emax3[j][k]):
                    rg = 1
                    N_rg=N_rg+1
                elif (Emax2[j][k]>Emax1[j][k]) and (Emax2[j][k]>Emax3[j][k]):
                    rg = 1
                    N_rg=N_rg+1
                if rg and (Emax1[j][k]<thresh):
                    N_brg=N_brg+1
            else:
                EdgeMap[j].append(0)
     
    per=float(N_da)/N_edge
    BlurExtent=float(N_brg)/N_rg
     
    if per>MinZero:
        print('Not blurred')
    else:
        print('Blurred')
        print('BlurExtent: ' + str(BlurExtent)) 
        
def equalize(im):
    h = im.convert("L").histogram()
    lut = []
    for b in range(0, len(h), 256):
      #step size
      step = reduce(operator.add, h[b:b+256])/255
      #create equalization lookup table
      n=0
      for i in range(256):
        lut.append(n/step)
        n=n+h[i+b]
    #map image through LUT
    #return im.point(lut*im.layers)
    return im.point(lut*3)


def claheApply(im):
  '''
  This function applies Contrast Limited Adaptive Histogram Equalization on each 
  of the R, G and B channels for the input image and recombines the channels to 
  produce a histogram equalized representation of the original color image.
  It calls the claheApply function to apply the histogram equalization technique.
  '''
  cv2img=np.array(im)    
  clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
  cv2img[:,:,0] = clahe.apply(cv2img[:,:,0])
  cv2img[:,:,1] = clahe.apply(cv2img[:,:,1])
  cv2img[:,:,2] = clahe.apply(cv2img[:,:,2])
  pilImg = Image.fromarray(cv2img)
  return pilImg


def findsubsets(S,m): 
	return (itertools.combinations(S,m))

def PFD(IN, filenames, TextOut, PropFace, ImpList, Cluster, ClusterProb, libsvm):
#for IN in range(0, N):
#try:	
	t1= time()
	line = filenames[IN]
	InFileName = line.split("/")[-1].split("\n")[0]  ### for UMD Dataset
	InFile = line.split('\n')[0] #InImgFolder+line.split("/umd_Annotate/")[-1].replace('.txt', '.jpg').split('\n')[0]
	#print InFile
	#img = cv2.imread(i);
	#im=Image.open(i).convert('F')
	#is_blur(im)

	img = Image.open(InFile)#.convert('RGB')

	(w, h) = img.size
	if w>h:
		img = img.rotate(rotateVal, expand = 0)
    	#print img.size
	
	im=np.array(img)
	im = im[:, :, ::-1].copy() 
	
	im1 = im.copy()
	im2 = im.copy()

	im = sc.misc.imresize(im, resizeFact)
    	if PreProc==1:
		im =np.array(equalize(Image.fromarray(im)))
		im =np.array(claheApply(Image.fromarray(im)))
	
	pilImg = Image.fromarray(im)
	        
	imOrig = im.copy()
	imOrig2 = im.copy()
	imVJ = im.copy()
	
	AllBoxes=[]
	AllParts =[]
	PartTag = []
	impFact = []
	midP=[]
	flag =0;
	
	
	if FlagDict['VJ']==1:
		#imheight, imwidth = img.shape() 
		threshold= 1+resizeFact/4
		minneighbours= PNeigh['VJ']        
		windowName = 'face_VJFrontal'
		flag, face_VJFrontal = facedetect_Upal(im, windowName, face_cascade_frontal, flag, (0, 250, 0), threshold, minneighbours, (int(250/(scaleFact)), int(250/(scaleFact))));
		for i in face_VJFrontal:
		    AllParts.append(i)
		    PartTag.append(PTag['VJ'])
		    AllBoxes.append(i)
		    xmid=(i[0]+i[2])/2
		    ymid=(i[1]+i[3])/2
		    midP.append([xmid, ymid])
		    impFact.append(1)
	    

	if len(AllBoxes)>=0:
    	  
		if FlagDict['VJProf']==1:	
		  threshold= 1.01
		  minneighbours= PNeigh['VJProf']       
		  windowName = 'face_VJprofile'
		  flag, face_VJprofile = facedetect_Upal(im, windowName, face_cascade_profile, flag, (0, 250, 250), threshold, minneighbours, (int(250/(scaleFact)), int(250/(scaleFact))));
		  for i in face_VJprofile:
		      AllParts.append(i)
		      PartTag.append(PTag['VJProf'])
		      AllBoxes.append(i)
		      xmid=(i[0]+i[2])/2
		      ymid=(i[1]+i[3])/2
		      midP.append([xmid, ymid])
		      impFact.append(2)

		if FlagDict['U12']==1:		   	
		#### VJ face detector for detecting Upper Half of the face. 
		  threshold= 1+resizeFact/4
		  minneighbours= PNeigh['U12']         
		  windowName = 'face_U_1_2'
		  flag, face_U_1_2 = facedetect_Upal(im, windowName, face_cascade_U_1_2, flag, (0, 200, 0), threshold, minneighbours, (int(128/(scaleFact)), int(66/(scaleFact))));
		  for i in face_U_1_2:
		      '''
		      IMPORTANT: The following variables need to be carefully assessed:
			      1. AllParts: this is the list of bounding boxes all the different parts detected by different classifier.
			      2. PartTag: this is the list of the unique tags of each part corresponding to AllParts, e.g. full face is tagged as 1, upper half face is tagged as 2 etc.
			      3. AllBoxes: this is the list of bounding boxes corresponding to full faces which are generated by expanding the bounding boxes of parts. For example,
			                      the upper half face bounding box is expanded vertically to assume the full face.
			      4. impFact: Importance Factor assigned to each part empirically. Intuitively, a full face has higher importance than a partial one. The importance factor
			                      for two different parts can be same. For example, upper left half and upper right half are both equally important. The importance factor
			                      if utilized int eh nms_Upal.py function to determine which face proposal is more likely. The lower the value of the impFact, the more important
			                      that part is. One thing to remember while setting the impFact values is how robust the classifier is. If a classifier is very robust and does
			                      not give false positives, then it should have the lowest impFact to have maximum impact if that classifier returns a bounding box.
		      '''
		      AllParts.append(i)
		      PartTag.append(PTag['U12'])
		      x11=i[0]
		      y11=i[1]
		      x12=i[2]
		      xmid=(i[0]+i[2])/2
		      ymid=i[3]
		      midP.append([xmid, ymid])
		      y12=min(int(i[3]+(i[3]-i[1])*0.8), im.shape[0])
		      AllBoxes.append([x11, y11, x12, y12])
		      impFact.append(2)
		

		if FlagDict['EyePar']==1:	
		  threshold= 1+resizeFact/4
		  minneighbours= PNeigh['EyePar']         
		  windowName = 'face_eyepair'
		  flag, face_eyepair = facedetect_Upal(im, windowName, face_cascade_eyepair, flag, (0, 50, 200), threshold, minneighbours, (int(11/(scaleFact)), int(45/(scaleFact))));
		  for i in face_eyepair:
		      AllParts.append(i)
		      PartTag.append(PTag['EyePar'])
		      x11=i[0]
		      y11=max(i[1]-(i[3]-i[1]), 0)
		      x12=i[2]
		      y12=min(i[3]+3*(i[3]-i[1]), im.shape[0])
		      AllBoxes.append([x11, y11, x12, y12])
		      xmid=(i[0]+i[2])/2
		      ymid=min(int(i[3]+(i[3]-i[1])/2), im.shape[0])
		      midP.append([xmid, ymid])
		      impFact.append(1)
		


			  

		if FlagDict['U34']==1:	
		  threshold= 1+resizeFact/4
		  minneighbours= PNeigh['U34']        
		  windowName = 'face_U_3_4'
		  flag, face_U_3_4 = facedetect_Upal(im, windowName, face_cascade_U_3_4, flag, (0, 0, 200), threshold, minneighbours, (int(128/(scaleFact)), int(98/(scaleFact))));
		  for i in face_U_3_4:
		      AllParts.append(i)
		      PartTag.append(PTag['U34'])
		      x11=i[0]
		      y11=i[1]
		      x12=i[2]
		      y12=min(int(i[3]+(i[3]-i[1])*0.3), im.shape[0])
		      AllBoxes.append([x11, y11, x12, y12])
		      xmid=(i[0]+i[2])/2
		      ymid=min(int(i[1]+(i[3]-i[1])*0.67), im.shape[0])
		      midP.append([xmid, ymid])
		      impFact.append(1)
	
		  
		if FlagDict['Nose']==1:	
		  threshold= 1+resizeFact/4
		  minneighbours=PNeigh['Nose']        
		  windowName = 'Nose'
		  flag, face_Nose = facedetect_Upal(im, windowName, face_cascade_nose, flag, (100, 0, 100), threshold, minneighbours, (int(72/(scaleFact)), int(60/(scaleFact))));
		  for i in face_Nose:
		      AllParts.append(i)
		      PartTag.append(PTag['Nose'])
		      x11=max(int(i[0]-(i[2]-i[0])*0.8), 0)
		      y11=max(int(i[1]-(i[3]-i[1])*0.8), 0)
		      x12=min(int(i[2]+(i[2]-i[0])*0.8), im.shape[1])
		      y12=min(int(i[3]+(i[3]-i[1])*0.8), im.shape[0])
		      AllBoxes.append([x11, y11, x12, y12])
		      xmid=(i[0]+i[2])/2
		      ymid=(i[1]+i[3])/2
		      midP.append([xmid, ymid])
		      impFact.append(1)


		if FlagDict['B12']==1:	            
		  threshold= 1+resizeFact/4
		  minneighbours= 3        
		  windowName = 'face_B_1_2'
		  flag, face_B_1_2 = facedetect_Upal(im, windowName, face_cascade_B_1_2, flag, (0, 200, 100), threshold, minneighbours, (int(128/(2*scaleFact)), int(66/(2*scaleFact))));
		  for i in face_U_1_2:
		      AllParts.append(i)
		      PartTag.append(PTag['B12'])
		      x11=i[0]
		      y11=max(int(i[1]-(i[3]-i[1])*0.8), 0)
		      x12=i[2]
		      y12=i[3]
		      AllBoxes.append([x11, y11, x12, y12])
		      xmid=(i[0]+i[2])/2
		      ymid=i[1]
		      midP.append([xmid, ymid])
		      impFact.append(3)
			
		if FlagDict['UL34']==1:	               
		  threshold= 1+resizeFact/4
		  minneighbours= PNeigh['UL34']        
		  windowName = 'face_UL_3_4'
		  imfl = im.copy()
		  imfl = np.fliplr(imfl)  # flip left-right and run the L12 cascade
		  flag, face_UL_3_4 = facedetect_Upal(imfl, windowName, face_cascade_UL_3_4, flag, (200, 0, 0), threshold, minneighbours, (int(98/(scaleFact)), int(98/(scaleFact))));
		  for i in face_UL_3_4:
		      temp=i.copy()
		      i[0] = imfl.shape[1]-temp[2]
		      i[2] = imfl.shape[1]-temp[0]
		      AllParts.append(i)
		      PartTag.append(PTag['UL34'])
		      x11=i[0]
		      y11=i[1]
		      x12=min(im.shape[1], int(i[2]+(i[2]-i[0])*0.3))
		      y12=min(int(i[3]+(i[3]-i[1])*0.3), im.shape[0])
		      AllBoxes.append([x11, y11, x12, y12])
		      xmid=min(int(i[0]+(i[2]-i[0])*0.67), im.shape[1])
		      ymid=min(int(i[1]+(i[3]-i[1])*0.67), im.shape[0])
		      midP.append([xmid, ymid])
		      impFact.append(2)
			     

		if FlagDict['UR34']==1:	
		  threshold= 1+resizeFact/4
		  minneighbours= PNeigh['UR34']        
		  windowName = 'face_UR_3_4'
		  flag, face_UR_3_4 = facedetect_Upal(im, windowName, face_cascade_UR_3_4, flag, (0, 200, 200), threshold, minneighbours, (int(98/(scaleFact)), int(98/(scaleFact))));
		  for i in face_UR_3_4:
		      AllParts.append(i)
		      PartTag.append(PTag['UR34'])
		      x11=max(int(i[0]-(i[2]-i[0])*0.3), 0)
		      y11=i[1]
		      x12=i[2]
		      y12=min(int(i[3]+(i[3]-i[1])*0.3), im.shape[0])
		      AllBoxes.append([x11, y11, x12, y12])
		      xmid=min(int(i[0]+(i[2]-i[0])*0.33), im.shape[1])
		      ymid=min(int(i[1]+(i[3]-i[1])*0.67), im.shape[0])
		      midP.append([xmid, ymid])
		      impFact.append(2)
	 
		if FlagDict['L12']==1:	               
		  threshold= 1+resizeFact/4
		  minneighbours= PNeigh['L12']       
		  windowName = 'face_L_1_2'
		  imfl = im.copy()
		  imfl = np.fliplr(imfl)  # flip left-right and run the L12 cascade
		  flag, face_L_1_2 = facedetect_Upal(imfl, windowName, face_cascade_L_1_2, flag, (200, 0, 200), threshold, minneighbours, (int(66/(scaleFact)), int(128/(scaleFact))));
		  for i in face_L_1_2:
		      temp=i.copy()
		      i[0] = imfl.shape[1]-temp[2]
		      i[2] = imfl.shape[1]-temp[0]
		      AllParts.append(i)
		      PartTag.append(PTag['L12'])
		      x11=i[0]
		      y11=i[1]
		      x12=min(im.shape[1], int(i[2]+(i[2]-i[0])*0.8))
		      y12=i[3]
		      AllBoxes.append([x11, y11, x12, y12])   
		      xmid=i[2]
		      ymid=int((i[1]+i[3])/2)
		      midP.append([xmid, ymid])      
		      impFact.append(3)
			      
		if FlagDict['R12']==1:	
		  threshold= 1+resizeFact/4
		  minneighbours= PNeigh['R12']       
		  windowName = 'face_R_1_2'
		  flag, face_R_1_2 = facedetect_Upal(im, windowName, face_cascade_R_1_2, flag, (200, 200, 0), threshold, minneighbours, (int(66/(scaleFact)), int(128/(scaleFact))));
		  for i in face_R_1_2:
		      AllParts.append(i)
		      PartTag.append(PTag['R12'])
		      x11=max(int(i[0]-(i[2]-i[0])*0.8), 0)
		      y11=i[1]
		      x12=i[2]
		      y12=i[3]
		      AllBoxes.append([x11, y11, x12, y12])
		      xmid=i[0]
		      ymid=int((i[1]+i[3])/2)
		      midP.append([xmid, ymid])
		      impFact.append(3)
	      
		if FlagDict['UL12']==1:	
		  threshold= 1+resizeFact/4
		  minneighbours= PNeigh['UL12']     
		  windowName = 'face_UL_1_2'
		  imfl = im.copy()
		  imfl = np.fliplr(imfl) # flip left-right and run the UR12 cascade
		  flag, face_UL_1_2 = facedetect_Upal(imfl, windowName, face_cascade_UL_1_2, flag, (200, 200, 200), threshold, minneighbours, (int(66/(scaleFact)), int(66/(scaleFact))));
		  for i in face_UL_1_2:
		      #print i
		      temp=i.copy()
		      i[0] = imfl.shape[1]-temp[2]
		      i[2] = imfl.shape[1]-temp[0]
		      #print 'flipped ', i
		      #print imfl.shape
		      AllParts.append(i)
		      PartTag.append(PTag['UL12'])
		      x11=i[0]
		      y11=i[1]
		      x12=min(im.shape[1], int(i[2]+(i[2]-i[0])*0.8))
		      y12=min(int(i[3]+(i[3]-i[1])*0.9), im.shape[0])
		      AllBoxes.append([x11, y11, x12, y12])
		      xmid=i[2]
		      ymid=i[3]
		      midP.append([xmid, ymid])
		      impFact.append(4)

		if FlagDict['UR12']==1:	                
		  threshold= 1+resizeFact/4
		  minneighbours= PNeigh['UR12']      
		  windowName = 'face_UR_1_2'
		  flag, face_UR_1_2 = facedetect_Upal(im, windowName, face_cascade_UR_1_2, flag, (100, 100, 250), threshold, minneighbours, (int(66/(scaleFact)), int(66/(scaleFact))));
		  for i in face_UR_1_2:
		      AllParts.append(i)
		      PartTag.append(PTag['UR12'])
		      x11=max(int(i[0]-(i[2]-i[0])*0.8), 0)
		      y11=i[1]
		      x12=i[2]
		      y12=min(int(i[3]+(i[3]-i[1])*0.8), im.shape[0])
		      AllBoxes.append([x11, y11, x12, y12])  
		      xmid=i[0]
		      ymid=i[3]
		      midP.append([xmid, ymid])          
		      impFact.append(4)

		if FlagDict['L34']==1:	
		  threshold= 1+resizeFact/4
		  minneighbours= PNeigh['L34']        
		  windowName = 'face_L_3_4'
		  imfl = im.copy()
		  imfl = np.fliplr(imfl) # flip left-right and run the UR12 cascade
		  flag, face_L_3_4 = facedetect_Upal(imfl, windowName, face_cascade_L_3_4, flag, (125, 250, 200), threshold, minneighbours, (int(128/(scaleFact)), int(98/(scaleFact))));
		  for i in face_L_3_4:
		      temp=i.copy()
		      i[0] = imfl.shape[1]-temp[2]
		      i[2] = imfl.shape[1]-temp[0]
		      AllParts.append(i)
		      PartTag.append(PTag['L34'])
		      x11=i[0]
		      y11=i[1]
		      x12=min(int(i[2]+(i[2]-i[0])*0.3), im.shape[1])
		      y12=i[3]
		      AllBoxes.append([x11, y11, x12, y12])
		      xmid=min(int(i[0]+(i[2]-i[0])*0.67), im.shape[1])
		      ymid=(i[1]+i[3])/2 
		      midP.append([xmid, ymid])
		      impFact.append(1)			 


		if FlagDict['R34']==1:	
		  threshold= 1+resizeFact/4
		  minneighbours= PNeigh['R34']         
		  windowName = 'face_R_3_4'
		  flag, face_R_3_4 = facedetect_Upal(im, windowName, face_cascade_R_3_4, flag, (250, 250, 125), threshold, minneighbours, (int(128/(scaleFact)), int(98/(scaleFact))));
		  for i in face_R_3_4:
		      AllParts.append(i)
		      PartTag.append(PTag['R34'])
		      x11=max(int(i[0]-(i[2]-i[0])*0.3), 0)
		      y11=i[1]
		      x12=i[2]
		      y12=i[3]
		      AllBoxes.append([x11, y11, x12, y12])
		      xmid=max(int(i[2]-(i[2]-i[0])*0.67), 0) 
		      ymid=(i[1]+i[3])/2 
		      midP.append([xmid, ymid])
		      impFact.append(1)	


		if FlagDict['B34']==1:	
		  threshold= 1+resizeFact/4
		  minneighbours= PNeigh['B34']       
		  windowName = 'face_B_3_4'
		  flag, face_B_3_4 = facedetect_Upal(im, windowName, face_cascade_B_3_4, flag, (125, 125, 200), threshold, minneighbours, (int(98/(scaleFact)), int(128/(scaleFact))));
		  for i in face_B_3_4:
		      AllParts.append(i)
		      PartTag.append(PTag['B34'])
		      x11=i[0]
		      y11=max(int(i[1]-(i[3]-i[1])*0.3), 0)
		      x12=i[2]
		      y12=i[3]
		      AllBoxes.append([x11, y11, x12, y12])
		      xmid=(i[0]+i[2])/2
		      ymid=max(int(i[3]-(i[3]-i[1])*0.67), 0)
		      midP.append([xmid, ymid])
		      impFact.append(1)

		z=np.asarray(AllBoxes)
		zP=np.asarray(AllParts)
		zMid=np.asarray(midP)
		bbox = []
		crop_img = []
		crop_propose=[] 
		FullBound = []
		PartDict=[]
		PickedPartDict=[]  
		LEye = []
		REye = []

	#pdb.set_trace()
	tempProp=[]
	Prob=0.00
	if len(z)>0:
		#### clustering and object proposals based on facial parts returned by the set of classifiers 
		FullBound, pick, PartDict, PickedPartDict, LEye, REye = Face_Cluster(im1, scaleFact, z, zMid, impFact, zP, PartTag, distThresh, olapThresh, impThresh, LEye, REye, fiduFlag, INTe)
		
		if PartDict!=[]:
			PartListTemp=[]
			FeatureList=[]
			
			for k in PartDict:
				PartListTemp.append(sorted([p for p in PartDict[k]['Parts']]))
				#pdb.set_trace()
				#PartDist=PartDict[k]['Dist']
			  	x1,y1,x2,y2= PartDict[k]['BBound']
				#PDist=[]
				#for pt in PartDist:
				#	PDist.append(PartDist[pt])
				#AvgDist=np.mean(PDist)

				#print x1, y1, x2, y2
				#crop_propose.append(im2[y1*scaleFact:y2*scaleFact, x1*scaleFact:x2*scaleFact])
				bbox=(map(lambda x: int(x*scaleFact), [x1,y1,x2,y2]))    
				#whRatio=(int(bbox[3])-int(bbox[1]))*1.0/(int(bbox[2])-int(bbox[0]))
				#area=(int(bbox[2])-int(bbox[0]))*(int(bbox[3])-int(bbox[1]))
				
				#IFactSum=0
				#IFactProd=0
				#for Parts in PartListTemp[-1]: 
				#	IFactSum+=ImpList['RevisedSum'][Parts]	
				#	IFactProd+=ImpList['RevisedProd'][Parts]
				
				indx=Cluster.index(PartListTemp[-1])
				#FeatureList.append([ClusterProb[indx][0], ClusterProb[indx][1], ClusterProb[indx][2], ClusterProb[indx][3], ClusterProb[indx][4], len(PartListTemp[-1]), whRatio, area]) #, AvgDist, area])
				#ft=([ClusterProb[indx][0], ClusterProb[indx][1], ClusterProb[indx][4]]) #, AvgDist, area])
				ft=([ClusterProb[indx][0], ClusterProb[indx][1]]) #, AvgDist, area])						
				ft.extend(ClusterProb[indx][5:]) #, AvgDist, area])
				FeatureList.append(ft)
				#if len(FeatureList[-1])<31:
				#	pdb.set_trace()
				#print FeatureList[-1]
				fidDictPrntProp = str(bbox[0])+','+str(bbox[1])+','+str(bbox[2])+','+str(bbox[3])
				TextTemp = InFile+ '\t'+fidDictPrntProp+'\t'
				for parts in PartDict[k]['Parts'].keys():
					TextTemp=TextTemp+parts+'\t'
				TextTemp=TextTemp[:-1]+'\n'
				#print TextTemp
				tempProp.append(TextTemp)
	
			#pdb.set_trace()
			Score=[]
			for mm in FeatureList:
				#print mm
				Score.append(libsvm.decision_function(mm))
		
			#pdb.set_trace()

			#print PartListTemp[sorted(PartSetIndx)[0][1]], PrioList[sorted(PartSetIndx)[0][0]]
			PSelIndx=Score.index(max(Score))
			Prob=max(Score)
		  	x1,y1,x2,y2= PartDict[PSelIndx]['BBound']
			
			#pdb.set_trace()
			#print PartDict[PSelIndx]['Parts']
			bbox=(map(lambda x: int(x*scaleFact), [x1,y1,x2,y2]))    
			fidDictPrnt = str(bbox[0])+','+str(bbox[1])+','+str(bbox[2])+','+str(bbox[3])
			crop_img=im2[bbox[1]:bbox[3], bbox[0]:bbox[2]]
			
			cv2.rectangle(im1, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (0, 255, 0), 2)
			windowName='Crpped_Face'
			cv2.imshow(windowName, crop_img)
			for n1, p1 in PartDict[PSelIndx]['Parts'].iteritems():
				cv2.rectangle(im1, (p1['BBox'][0], p1['BBox'][1]), (p1['BBox'][2], p1['BBox'][3]), (0, 255, 255), 2)       
				partCrop = im2[p1['BBox'][1]:p1['BBox'][3], p1['BBox'][0]:p1['BBox'][2]]
				windowName=n1
				if plot ==1:
					cv2.imshow(windowName, partCrop)
				if ImgSave==1:
					partName=n1
					FileName= InFile.split('\n')[0].split('/')[-1]
					
					OutImgName = OutFolder+"CroppedFull_TestDevel_Comb"+combNo+"/"+str(scaleFact)+"/"+InFile.split('\n')[0].split('/TestImages')[1]
					if not os.path.exists(OutImgName.split('frame_')[0]+partName+'/'):
						os.makedirs(OutImgName.split('frame_')[0]+partName+'/')
					cv2.imwrite(OutImgName.split('frame_')[0]+partName+'/'+FileName, partCrop)
			if ImgSave==1:
				partName='Full'
				FileName= InFile.split('\n')[0].split('/')[-1]
				
				OutImgName = OutFolder+"CroppedFull_TestDevel_Comb"+combNo+"/"+str(scaleFact)+"/"+InFile.split('\n')[0].split('/TestImages')[1]
				if not os.path.exists(OutImgName.split('frame_')[0]+partName+'/'):
					os.makedirs(OutImgName.split('frame_')[0]+partName+'/')
				cv2.imwrite(OutImgName.split('frame_')[0]+partName+'/'+FileName, crop_img)
			

			'''
			if ImgSave==1:
				OutImgName = OutFolder+"CroppedFull_TestDevel_Comb"+combNo+"/"+str(scaleFact)+"/"+str(IN)+'_'+InFile.replace('/','_').split('\n')[0][1:]
				if not os.path.exists(OutFolder+"CroppedFull_TestDevel_Comb"+combNo+"/"+str(scaleFact)+"/"):
					os.makedirs(OutFolder+"CroppedFull_TestDevel_Comb"+combNo+"/"+str(scaleFact)+"/")
				cv2.imwrite(OutImgName, im1)
			'''		

		else:
			fidDictPrntProp='NoFace'
			TextTemp = InFile+ '\t'+fidDictPrntProp+'\n'
			tempProp.append(TextTemp)
			#pdb.set_trace()
			fidDictPrnt='NoFace'
			if ImgSave==1:
				partName='Full'
				FileName= InFile.split('\n')[0].split('/')[-1]
				
				OutImgName = OutFolder+"NoFace_TestDevel_Comb"+combNo+"/"+str(scaleFact)+"/"+InFile.split('\n')[0].split('/TestImages')[1]
				if not os.path.exists(OutImgName.split('frame_')[0]+partName+'/'):

					os.makedirs(OutImgName.split('frame_')[0]+partName+'/')
				cv2.imwrite(OutImgName.split('frame_')[0]+partName+'/'+FileName, im1)

		#cv2.waitKey()		
		#pdb.set_trace()
		if plot == 1:
			windowName="After NMS"
  	        	cv2.namedWindow(windowName, cv2.WINDOW_NORMAL)
  	        	cv2.resizeWindow(windowName, im1.shape[1]/2, im1.shape[0]/2)
		 	cv2.imshow(windowName, im1)
		 	if len(crop_img)>0:      
				windowName="cropped"
				cv2.imshow(windowName, crop_img)
				cv2.waitKey()
		#pdb.set_trace()
	else:
		#cv2.imshow('Img', im1)
		#cv2.waitKey()
		fidDictPrnt = 'NoFace'
		fidDictPrntProp='NoFace'
		TextTemp = InFile+ '\t'+fidDictPrntProp+'\n'
		tempProp.append(TextTemp)

		if ImgSave==1:
			OutImgName=OutFolder+"NoFace_TestDevel_Comb"+combNo+"/"+str(scaleFact)+"/"+str(IN)+'_'+InFile.replace('/', '_').split('\n')[0][1:]
	    	        if not os.path.exists(OutFolder+"NoFace_TestDevel_Comb"+combNo+"/"+str(scaleFact)+"/"):
				os.makedirs(OutFolder+"NoFace_TestDevel_Comb"+combNo+"/"+str(scaleFact)+"/")
			cv2.imwrite(OutImgName, im1)
		#continue

	T='%.2f' % (time() - t1)
	#print('Processing time:  %.2fs.' % (T))

	printTxt= InFile+ '\t'+fidDictPrnt+'\t%0.5f\t%s\n' %(Prob, T)
	print IN, '/', N, ' : ', printTxt[:-1]
	#pdb.set_trace()
	TextOut[IN]=printTxt
	PropFace[IN]=tempProp

	#pdb.set_trace()
	#cv2.destroyAllWindows() 
#except:
#	print "Unexpected Error"
#	continue


    
def DistHelper(args):
	return PFD(*args);
    
if __name__ == "__main__":
    fileList = open(InFileList, 'r')
    filenames = [x.split('\t')[0] for x in fileList]
    N = len(filenames)

    Cluster=[]
    ClusterProb=[]    
    fpri=open(ImpFile, 'r')
    ImpList=pickle.load(fpri)

    Cluster=ImpList['UniquePartSet']
    ClusterProb=ImpList['UniquePartProb']


    fsvm=open(SVMFile, 'r')
    SVM=pickle.load(fsvm)

    ### Get images parallely
    p=Pool(processNo)
    manager= Manager()
    TextOut = manager.dict()
    PropFace= manager.dict()
    job_args = [(IN, filenames, TextOut, PropFace, ImpList, Cluster, ClusterProb, SVM) for IN in range(0,N)]

    t0= time()
    if pooling == 1:
	p.map(DistHelper, job_args)
    else:
	map(DistHelper, job_args)

    p.close()
    #pdb.set_trace()
    print('Total Time : %.2fs for %d images --> Time per image:  %.2fs.' % ((time() - t0), N, (time() - t0)/float(N)))

    f= open(OutFileName,'w')
    for k in TextOut.keys():
	printTxt=TextOut[k]
	f.write(printTxt)
	
    f.close()

    cv2.destroyAllWindows()

