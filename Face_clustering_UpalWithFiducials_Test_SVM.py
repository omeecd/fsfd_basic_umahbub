# -*- coding: utf-8 -*-
"""
Created on Wed Feb 11 14:02:12 2015

@author: Upal Mahbub, University of Maryland, College Park
         umahbub@umd.edu
"""

# import the necessary packages
import numpy as np
import math
#from nms import non_max_suppression_slow
import cv2
from PIL import Image
import itertools
import pdb
import random

face_cascade_LEye = cv2.CascadeClassifier('/cfarhomes/umahbub/opencv_src/opencv/data/haarcascades/haarcascade_lefteye_2splits.xml')
face_cascade_REye = cv2.CascadeClassifier('/cfarhomes/umahbub/opencv_src/opencv/data/haarcascades/haarcascade_lefteye_2splits.xml')

plot = 0

def detectEye(im, Part, face_cascade_eye, minNeigh, minSize):
        scFact=0.25
	#print im.shape
        #colorIm=cv2.resize(im, (0,0), fx=scFact, fy=scFact)
	colorIm = im.copy()
        gray = cv2.cvtColor(colorIm, cv2.COLOR_RGB2GRAY)
        eyes = face_cascade_eye.detectMultiScale(gray, 1+scFact/2, minNeigh, 0, (int(minSize[0]), int(minSize[1])))
        #print 'Eyes', eyes
        if len(eyes)>0:
                boundingBox=np.empty([len(eyes), 4], dtype = int)
                i = 0
                for (x,  y, w, h) in (eyes):
                      boundingBox[i:]= (x, y, (x+w), (y+h))
                      i=i+1
                newImg= colorIm.copy()

                for (x,  y, w, h) in (eyes):
                        cv2.circle(newImg, (int(x+w/2), int(y+h/2)), 2, (0, 0, 255), 2)
                        if plot == 1:
                                cv2.rectangle(newImg, (x,y), (x+w, y+h), (255,0,100), 2)
                if plot==1:
                        cv2.imshow(Part, newImg);
                #print 'EyeBounds', boundingBox
                return boundingBox
        else:
                return []
                
def removearray(L,arr):
    ind = 0
    size = len(L)
    while ind != size and not np.array_equal(L[ind],arr):
        ind += 1
    if ind != size:
        L.pop(ind)
    else:
        raise ValueError('array not found in list.')


def entropy(signal):
        '''
        function returns entropy of a signal - signal must be 1-D numpy array
        '''
        lensig = signal.size
        symset = list(set(signal))
        numsys=len(symset)
        propab = [np.size(signal[signal==i])/(1.0*lensig) for i in symset]
        ent = np.sum([p*np.log2(1.0/p) for p in propab])
        return ent
        

def getEntropy(im, Part):
        import matplotlib.pyplot as plt
        
        colorIm=cv2.resize(im, (0,0), fx=0.125, fy=0.125)
        grayIm = cv2.cvtColor(colorIm, cv2.COLOR_BGR2GRAY)
        N=5
        S=grayIm.shape
        E=np.array(grayIm)
        for row in range(S[0]):
                for col in range(S[1]):
                        Lx=np.max([0, col-N])
                        Ux=np.min([S[1], col+N])
                        Ly=np.max([0, row-N])
                        Uy=np.min([S[0], row+N])
                        region = grayIm[Ly:Uy, Lx:Ux].flatten()
                        E[row,col]=entropy(region)
         
        #cv2.imshow('Entropy', E)
                

        plt.figure(num = None, figsize=(14,7))
        plt.subplot(1,3,1)
        plt.imshow(colorIm)
        
        plt.subplot(1,3,2)
        plt.imshow(grayIm, cmap=plt.cm.gray)
        
        plt.subplot(1,3,3)
        plt.imshow(E, cmap=plt.cm.jet)
        plt.xlabel('Entropy in 10x10 neighbourhood')
        plt.colorbar(shrink = 0.5)
        plt.show()
        #plt.waitforbuttonpress()


'''
def getEntropy(image, Part):
        import matplotlib.pyplot as plt
        from skimage import data
        from skimage.filters.rank import entropy 
        from skimage.morphology import disk
        from skimage.util import img_as_ubyte
        
        img = img_as_ubyte(image)
        fig, (ax0, ax1) = plt.subplots(ncols=2, figsize=(10, 4))
        img0 = ax0.imshow(img, cmap = plt.cm.gray)
        ax0.set_title(Part)
        ax0.axis('off')
        fig.colorbar(img0, ax=ax0)
        eye
        img1=ax1.imshow(entropy(img, disk(5)), cmap=plt.cm.jet)
        ax1=set_title('Entropy')
        ax1.axis('off')
        fig.colorbar(img1, ax=ax1)
        
        plt.show()
'''        
        



def adjustEyePos(LEye, REye, LEyeNew, REyeNew):
        if len(LEye)==0:
                LEye=LEyeNew
        elif len(LEyeNew) == 0:
                LEye = LEye
        else:
                LEye = [int(LEye[0]+LEyeNew[0])/2, int(LEye[1]+LEyeNew[1])/2]

        if len(REye)==0:
                REye=REyeNew
        elif len(REyeNew)==0:
                REye = REye
        else:
                REye = [int(REye[0]+REyeNew[0])/2, int(REye[1]+REyeNew[1])/2]
        return LEye, REye

def map2part(partIndx, box, PartTag, scaleFact, im1,fiduFlag):
    '''
    Builds a dictionary of the bounding boxes of each part in the proposal.
        partIndx is the index of the part in the array of bounding boxes (box)map
        box is the array of bounding boxes of parts for the given proposal
        PartTag is the uniqe tag of each part
        scaleFact is the scaling factor using which the image was resized. The bounding box parameters are multiplied by this factor to obtain 
                the bounding box for the full size image
    '''
    im2=im1.copy()
    PDict ={}
    #PartNames = ['', 'Full', 'U_1_2', 'LR_3_4', 'U_3_4', 'Nose', 'B_1_2', 'UL_3_4', 'UR_3_4', 'L_1_2', 'R_1_2', 'UL_1_2', 'UR_1_2']     
    PartNames = ['', 'Full', 'U_1_2', 'LR_3_4', 'U_3_4', 'Nose', 'B_1_2', 'UL_3_4', 'UR_3_4', 'L_1_2', 'R_1_2', 'UL_1_2', 'UR_1_2', 'EyePair', 'VJProf', 'L_3_4', 'R_3_4', 'B_3_4']
    count = 0
    
    LEye = ''
    REye = ''
    for i in partIndx:
        PDict[PartNames[PartTag[i]]]={}
        BBox = [int(k*scaleFact) for k in box[count]]
	PDict[PartNames[PartTag[i]]]['BBox']=BBox
	if fiduFlag==1:
		if PartNames[PartTag[i]]=='U_1_2':
		        PDict[PartNames[PartTag[i]]]['LEYE']=[int(BBox[0]+(BBox[2]-BBox[0])*0.25), int(BBox[1]+(BBox[3]-BBox[1])*0.55)]
		        PDict[PartNames[PartTag[i]]]['REYE']=[int(BBox[0]+(BBox[2]-BBox[0])*0.75), int(BBox[1]+(BBox[3]-BBox[1])*0.55)] 

		        LEye, REye = adjustEyePos(LEye, REye, PDict[PartNames[PartTag[i]]]['LEYE'], PDict[PartNames[PartTag[i]]]['REYE'])
		        
		        #BothEyeBox = detectEye(im2[BBox[1]:BBox[3], BBox[0]:BBox[2]], 'U_1_2')

		        
		        if plot ==1:
		                cv2.rectangle(im1, (PDict[PartNames[PartTag[i]]]['BBox'][0], PDict[PartNames[PartTag[i]]]['BBox'][1]), (PDict[PartNames[PartTag[i]]]['BBox'][2], PDict[PartNames[PartTag[i]]]['BBox'][3]), (0, 255, 255), 2)
		                cv2.circle(im1, (PDict[PartNames[PartTag[i]]]['LEYE'][0], PDict[PartNames[PartTag[i]]]['LEYE'][1]), 2, (0, 0, 255), 2)
		                cv2.circle(im1, (PDict[PartNames[PartTag[i]]]['REYE'][0], PDict[PartNames[PartTag[i]]]['REYE'][1]), 2, (0, 0, 255), 2)


		if PartNames[PartTag[i]]=='U_3_4':
		        PDict[PartNames[PartTag[i]]]['LEYE']=[int(BBox[0]+(BBox[2]-BBox[0])*0.25), int(BBox[1]+(BBox[3]-BBox[1])*0.4)]
		        PDict[PartNames[PartTag[i]]]['REYE']=[int(BBox[0]+(BBox[2]-BBox[0])*0.75), int(BBox[1]+(BBox[3]-BBox[1])*0.4)] 
		        
		        LEye, REye = adjustEyePos(LEye, REye, PDict[PartNames[PartTag[i]]]['LEYE'], PDict[PartNames[PartTag[i]]]['REYE'])
		        
		        #BothEyeBox = detectEye(im2[BBox[1]:BBox[3], BBox[0]:BBox[2]], 'U_3_4')
		        
		        if plot ==1:
		                cv2.rectangle(im1, (PDict[PartNames[PartTag[i]]]['BBox'][0], PDict[PartNames[PartTag[i]]]['BBox'][1]), (PDict[PartNames[PartTag[i]]]['BBox'][2], PDict[PartNames[PartTag[i]]]['BBox'][3]), (255, 100, 100), 2)
		                cv2.circle(im1, (PDict[PartNames[PartTag[i]]]['LEYE'][0], PDict[PartNames[PartTag[i]]]['LEYE'][1]), 2, (100, 0, 255), 2)
		                cv2.circle(im1, (PDict[PartNames[PartTag[i]]]['REYE'][0], PDict[PartNames[PartTag[i]]]['REYE'][1]), 2, (100, 0, 255), 2)
		
		        
		if PartNames[PartTag[i]]=='UL_3_4':
		        PDict[PartNames[PartTag[i]]]['LEYE']=[int(BBox[0]+(BBox[2]-BBox[0])*0.33), int(BBox[1]+(BBox[3]-BBox[1])*0.45)]
		        PDict[PartNames[PartTag[i]]]['REYE']='' 
		        
		        LEye, REye = adjustEyePos(LEye, REye, PDict[PartNames[PartTag[i]]]['LEYE'], PDict[PartNames[PartTag[i]]]['REYE'])
		        
		        #LEyeBox = detectEye(im2[BBox[1]:BBox[3], BBox[0]:BBox[2]], 'UL_3_4', face_cascade_LEye, 2.0)
		        
		        if plot ==1:
		                cv2.rectangle(im1, (PDict[PartNames[PartTag[i]]]['BBox'][0], PDict[PartNames[PartTag[i]]]['BBox'][1]), (PDict[PartNames[PartTag[i]]]['BBox'][2], PDict[PartNames[PartTag[i]]]['BBox'][3]), (100, 100, 100), 2)
		                cv2.circle(im1, (PDict[PartNames[PartTag[i]]]['LEYE'][0], PDict[PartNames[PartTag[i]]]['LEYE'][1]), 2, (100, 0, 100), 2)
		              
		if PartNames[PartTag[i]]=='UR_3_4':
		        PDict[PartNames[PartTag[i]]]['LEYE']='' 
		        PDict[PartNames[PartTag[i]]]['REYE']=[int(BBox[0]+(BBox[2]-BBox[0])*0.66), int(BBox[1]+(BBox[3]-BBox[1])*0.45)]
		        
		        LEye, REye = adjustEyePos(LEye, REye, PDict[PartNames[PartTag[i]]]['LEYE'], PDict[PartNames[PartTag[i]]]['REYE'])
		       
		        #REyeBox = detectEye(im2[BBox[1]:BBox[3], BBox[0]:BBox[2]], 'UR_3_4', face_cascade_REye, 2.0)
		        
		        if plot ==1:
		                cv2.rectangle(im1, (PDict[PartNames[PartTag[i]]]['BBox'][0], PDict[PartNames[PartTag[i]]]['BBox'][1]), (PDict[PartNames[PartTag[i]]]['BBox'][2], PDict[PartNames[PartTag[i]]]['BBox'][3]), (100, 200, 200), 2)
		                cv2.circle(im1, (PDict[PartNames[PartTag[i]]]['REYE'][0], PDict[PartNames[PartTag[i]]]['REYE'][1]), 2, (100, 0, 200), 2)






		if PartNames[PartTag[i]]=='L_1_2':
		        PDict[PartNames[PartTag[i]]]['LEYE']=[int(BBox[0]+(BBox[2]-BBox[0])*0.5), int(BBox[1]+(BBox[3]-BBox[1])*0.33)]
		        PDict[PartNames[PartTag[i]]]['REYE']='' 
		        
		        LEye, REye = adjustEyePos(LEye, REye, PDict[PartNames[PartTag[i]]]['LEYE'], PDict[PartNames[PartTag[i]]]['REYE'])
		        
		        LEyeBox = detectEye(im2[BBox[1]:BBox[3], BBox[0]:BBox[2]], 'L_1_2', face_cascade_LEye, 3, [((BBox[2]-BBox[0])*0.3), ((BBox[3]-BBox[1])*0.2)])
		        if len(LEyeBox)>0:
		                for xx in LEyeBox:
		                        boxMid =[int(BBox[0]+xx[0]+(xx[2]-xx[0])/2), int(BBox[1]+xx[1]+(xx[3]-xx[1])/2)]
		                        if boxMid[1]<int(BBox[1]+(BBox[3]-BBox[1])*0.4):
		                                LEye= ''
		                                LEye, REye = adjustEyePos(LEye, REye, boxMid, '')
		        
		        if plot ==1:
		                cv2.rectangle(im1, (PDict[PartNames[PartTag[i]]]['BBox'][0], PDict[PartNames[PartTag[i]]]['BBox'][1]), (PDict[PartNames[PartTag[i]]]['BBox'][2], PDict[PartNames[PartTag[i]]]['BBox'][3]), (100, 100, 100), 2)
		                #cv2.circle(im1, (PDict[PartNames[PartTag[i]]]['LEYE'][0], PDict[PartNames[PartTag[i]]]['LEYE'][1]), 2, (100, 0, 100), 2)
		              
		              
		              
		if PartNames[PartTag[i]]=='R_1_2':
		        PDict[PartNames[PartTag[i]]]['LEYE']='' 
		        PDict[PartNames[PartTag[i]]]['REYE']=[int(BBox[0]+(BBox[2]-BBox[0])*0.5), int(BBox[1]+(BBox[3]-BBox[1])*0.33)]
		        
		        LEye, REye = adjustEyePos(LEye, REye, PDict[PartNames[PartTag[i]]]['LEYE'], PDict[PartNames[PartTag[i]]]['REYE'])
		        
		        REyeBox = detectEye(im2[BBox[1]:BBox[3], BBox[0]:BBox[2]], 'R_1_2', face_cascade_REye, 3, [((BBox[2]-BBox[0])*0.3), ((BBox[3]-BBox[1])*0.2)])
		        if len(REyeBox)>0:                             
		                for xx in REyeBox:
		                        boxMid =[int(BBox[0]+xx[0]+(xx[2]-xx[0])/2), int(BBox[1]+xx[1]+(xx[3]-xx[1])/2)]
		                        if boxMid[1]<int(BBox[1]+(BBox[3]-BBox[1])*0.4):
		                                REye= ''
		                                LEye, REye = adjustEyePos(LEye, REye, '', boxMid)            
		                                    
		        if plot ==1:
		                cv2.rectangle(im1, (PDict[PartNames[PartTag[i]]]['BBox'][0], PDict[PartNames[PartTag[i]]]['BBox'][1]), (PDict[PartNames[PartTag[i]]]['BBox'][2], PDict[PartNames[PartTag[i]]]['BBox'][3]), (100, 200, 200), 2)
		                #cv2.circle(im1, (PDict[PartNames[PartTag[i]]]['REYE'][0], PDict[PartNames[PartTag[i]]]['REYE'][1]), 2, (100, 0, 200), 2)









		if PartNames[PartTag[i]]=='UL_1_2':
		        PitertoolsDict[PartNames[PartTag[i]]]['LEYE']=[int(BBox[0]+(BBox[2]-BBox[0])*0.33), int(BBox[1]+(BBox[3]-BBox[1])*0.5)]
		        PDict[PartNames[PartTag[i]]]['REYE']='' 
		        
		        LEye, REye = adjustEyePos(LEye, REye, PDict[PartNames[PartTag[i]]]['LEYE'], PDict[PartNames[PartTag[i]]]['REYE'])
		                        
		        LEyeBox = detectEye(im2[BBox[1]:BBox[3], BBox[0]:BBox[2]], 'UL_1_2', face_cascade_LEye, 2, [((BBox[2]-BBox[0])*0.3), ((BBox[3]-BBox[1])*0.3)])
		        if len(LEyeBox)>0:
		                for xx in LEyeBox:
		                        boxMid =[int(BBox[0]+xx[0]+(xx[2]-xx[0])/2), int(BBox[1]+xx[1]+(xx[3]-xx[1])/2)]
		                        if boxMid[1]>int(BBox[1]+(BBox[3]-BBox[1])*0.3) and boxMid[1]<int(BBox[1]+(BBox[3]-BBox[1])*0.7):
		                                LEye= ''
		                                LEye, REye = adjustEyePos(LEye, REye, boxMid, '')
		         
		        if plot ==1:
		                cv2.rectangle(im1, (PDict[PartNames[PartTag[i]]]['BBox'][0], PDict[PartNames[PartTag[i]]]['BBox'][1]), (PDict[PartNames[PartTag[i]]]['BBox'][2], PDict[PartNames[PartTag[i]]]['BBox'][3]), (0, 0, 100), 2)
		                #cv2.circle(im1, (PDict[PartNames[PartTag[i]]]['LEYE'][0], PDict[PartNames[PartTag[i]]]['LEYE'][1]), 2, (200, 0, 100), 2)
		              
		if PartNames[PartTag[i]]=='UR_1_2':
		        PDict[PartNames[PartTag[i]]]['LEYE']='' 
		        PDict[PartNames[PartTag[i]]]['REYE']=[int(BBox[0]+(BBox[2]-BBox[0])*0.66), int(BBox[1]+(BBox[3]-BBox[1])*0.5)]
		        
		        LEye, REye = adjustEyePos(LEye, REye, PDict[PartNames[PartTag[i]]]['LEYE'], PDict[PartNames[PartTag[i]]]['REYE'])
		        
		        REyeBox = detectEye(im2[BBox[1]:BBox[3], BBox[0]:BBox[2]], 'UR_1_2', face_cascade_REye, 2, [((BBox[2]-BBox[0])*0.2), ((BBox[3]-BBox[1])*0.2)])
		        if len(REyeBox)>0:      
		                for xx in REyeBox:
		                        boxMid =[int(BBox[0]+xx[0]+(xx[2]-xx[0])/2), int(BBox[1]+xx[1]+(xx[3]-xx[1])/2)]
		                        if boxMid[1]>int(BBox[1]+(BBox[3]-BBox[1])*0.3) and boxMid[1]<int(BBox[1]+(BBox[3]-BBox[1])*0.7):
		                                REye= ''
		                                LEye, REye = adjustEyePos(LEye, REye, '', boxMid)    
		                        
		        if plot ==1:
		                cv2.rectangle(im1, (PDict[PartNames[PartTag[i]]]['BBox'][0], PDict[PartNames[PartTag[i]]]['BBox'][1]), (PDict[PartNames[PartTag[i]]]['BBox'][2], PDict[PartNames[PartTag[i]]]['BBox'][3]), (200, 200, 200), 2)
		                #cv2.circle(im1, (PDict[PartNames[PartTag[i]]]['REYE'][0], PDict[PartNames[PartTag[i]]]['REYE'][1]), 2, (200, 200, 0), 2)
	 
        count = count+1
    
    if fiduFlag ==1:
	    if len(LEye)>0 and plot == 1:
		    cv2.circle(im1, (LEye[0], LEye[1]), 2, (200, 0, 50), 3)
	    if len(REye)>0 and plot == 1:
		    cv2.circle(im1, (REye[0], REye[1]), 2, (200, 0, 50), 3)

	    if plot == 1:
		    windowName="Fiducials From Parts"
		    cv2.imshow(windowName, im1)
		    #cv2.waitKey();
    
    return PDict, LEye, REye



def findsubsets(S,m): 
	return (itertools.combinations(S,m))
        
def Face_Cluster(im, scaleFact, boxes, mid, impFact, PartBoxes, PartTag, distThresh, olapThresh, impThresh, LEye, REye, fiduFlag, IN): ### Upal edit: impFact to denote the type of box   

    # if there are no boxes, return an empty list
    if len(boxes) == 0:
        return boxes.tolist(), [], [], [], [], []
    
    '''
    if 1 in PartTag:
	indx=[i for (i,j) in zip(range(0, len(PartTag)), PartTag) if j== 1]
	flag = 0
        #print "found 1"
	maxArea = 0
	for k in indx:
		w=boxes[k][2]-boxes[k][0]
		h=boxes[k][3]-boxes[k][1]
		area = w*h
		if area >=maxArea:
			PDict = {}
			PDict['Full']={}
			PDict['Full']['BBox']=[int(i*scaleFact) for i in boxes[k]]
			maxArea=area
			#print PDict
			flag = 1
	if flag == 1:
		return boxes.tolist(), [boxes[PartTag.index(1)].tolist()], PDict, PDict, LEye, REye
    '''

    # initialize the list of picked indexes
    pick = []
 

    # grab the coordinates of the bounding boxes
    x1 = boxes[:,0]
    y1 = boxes[:,1]
    x2 = boxes[:,2]
    y2 = boxes[:,3]
    
    # find the center of each bounding box. These are the nodes of the graph
    xmid = mid[:,0]#(x1+x2)/2
    ymid = mid[:,1]#(y1+y2)/2

    # find the width and heigh of each bounding box    
    xrad = (x2-x1)/2
    yrad = (y2-y1)/2
    

    
    ########### Find the clusters of parts around each bounding box and calculate the distances between Bounding boxes###########
    dist={}
    BboxArr={}
    PartArr={}   ## Contains the index of the parts of face detected
    PartBoundArr={}
    GroupImpFact1 = [float(j) for j in impFact]
    for i in range(len(xmid)):
        if i not in dist:        
            dist[i]={}
            BboxList=[]
            PartList=[]
            PartBound=[]
            BboxList.append(boxes[i, :])
            PartBound.append(PartBoxes[i,:])
            PartList.append(i)
        for j in range(len(xmid)):
            if PartTag[i]!=PartTag[j]:
                if PartTag[j] in dist[i]:
                    distanceIJ=math.sqrt((xmid[i]-xmid[j])**2+(ymid[i]-ymid[j])**2)
                    if (distanceIJ < dist[i][PartTag[j]][1]):
                        removearray(PartList, dist[i][PartTag[j]][0])
                        PartList.append(j)
                        removearray(BboxList, np.array(boxes[dist[i][PartTag[j]][0],:]))
                        removearray(PartBound, np.array(PartBoxes[dist[i][PartTag[j]][0],:]))
                        dist[i][PartTag[j]]= [j, distanceIJ]
                        BboxList.append(boxes[j,:])
                        PartBound.append(PartBoxes[j,:])

                else:
                    distanceIJ=math.sqrt((xmid[i]-xmid[j])**2+(ymid[i]-ymid[j])**2)
                    if distanceIJ< math.sqrt((xrad[i])**2+(yrad[i])**2)/(distThresh): ##max(xrad[i], yrad[i]):
                        dist[i][PartTag[j]]= [j, distanceIJ]
                        BboxList.append(boxes[j,:])
                        PartBound.append(PartBoxes[j,:])
                        PartList.append(j)
                        GroupImpFact1[i]=GroupImpFact1[i]+impFact[j]


        if len(BboxList)>0:
            BboxArr[i]={}
            PartArr[i]={}
            PartBoundArr[i]={}
            BboxArr[i]=np.array(BboxList)
            PartArr[i]=np.array(PartList)
            PartBoundArr[i]=np.array(PartBound)
        if i not in BboxArr:
            dist.pop(i, None)
    

    #### calculate the full face bounding box from parts for each face proposal. Also, make a list of bounding boxes of parts for each proposed face
    FullBound=[]
    olapNum=[]
    GroupImpFact=[]
    BetterParts = []
    PartSetList =[]	
    partBounds = []
    distN={}
    count = 0
    countSet = 0
    for node1, box1 in BboxArr.iteritems():
	if len(PartArr[count])>=olapThresh:
		PartListSubSet=set()
		for j in range(len(PartArr[count]), max(olapThresh-2, 0), -1):
			PartListSubSet.update(k for k in findsubsets(PartArr[count][1:], j))
	
		UniqueSubSet=set()
		Plist=list(PartListSubSet)
		random.shuffle(Plist)
		Ns=len(Plist)
		Plist[Ns-1]=(tuple(PartArr[count][1:]))
		cntRng=range(0, min(IN, Ns))

		for j in cntRng:
			s=Plist[j]
			partTagSubSet=[PartTag[l] for l in sorted(s)]
			s=sorted((PartArr[count][0],)+s)
		
			distN[countSet]={}
			boxSubSet=[boxes[l,:] for l in s]
			boxArr=np.array(boxSubSet)
			ImpFactSubSet=[impFact[l] for l in s]
			partBoxSubSet=[PartBoxes[l,:] for l in s]
			partBoundArr=np.array(partBoxSubSet)
			partArr=np.array(s)
		
			if partArr.tolist() not in PartSetList:
				z=[min(boxArr[:,0]), min(boxArr[:,1]), max(boxArr[:,2]), max(boxArr[:,3])]

				area = abs(z[2]-z[0])*abs(z[3]-z[1])
				#pdb.set_trace()
				if area >=(250/(scaleFact))*(250/(scaleFact)): #indxDel = [i for (i,j) in zip(range(0, len(area)), area) if j<15625]
					FullBound.append(z)
					olapNum.append(len(boxSubSet))
					GroupImpFact.append(float(sum(ImpFactSubSet))/len(boxSubSet))
					BetterParts.append(partArr)
					PartSetList.append(partArr.tolist())
					partBounds.append(partBoundArr)
					for pt in partTagSubSet:
						#pdb.set_trace()
						distN[countSet][pt]=dist[count][pt]
					countSet+=1
        count=count+1
    FullBoundArr=np.array(FullBound)    

    #pdb.set_trace()

    im1=im.copy()
    imFidu = im.copy()

    #### Calculate Mean Distance of the components of a face proposal from the center
    i=0
    MeanDist=np.zeros(len(distN))
    for node1, value1 in distN.iteritems():
        MeanDist[i]=0
        for node2, value2 in value1.iteritems():
            MeanDist[i]=MeanDist[i]+value2[1]
        if (len(value1)>0):
            MeanDist[i]=MeanDist[i]/len(value1)#*GroupImpFact[i]
        i=i+1

    #pdb.set_trace()
    ZeroMDist=[i for (i,j) in zip(range(0, len(MeanDist)), MeanDist) if j== 0]
    NonZeroMDist=[i for (i,j) in zip(range(0, len(MeanDist)), MeanDist) if j!= 0]
    NMDist = [j for j in MeanDist if j!= 0]    
    MeanDist[ZeroMDist]="inf"

    #MeanOlap = np.mean([olapNum[j] for j in NonZeroMDist])

    BestCandidateOlap = [i for (i,j) in zip(range(0, len(olapNum)), olapNum) if j>= olapThresh] #max(MeanOlap, olapThresh)]
    BestCandidatesOverall = set(BestCandidateOlap)


    if not BestCandidatesOverall:
        return boxes.tolist(), [], [], [], [], []


    BestBounds = []
    BestConf = []
    BestOlap = []
    BestGroupParts =[]
    BestPartBounds = []
    BestPartDist=[]
    for i in BestCandidatesOverall:
        BestBounds.append(FullBound[i])
        BestConf.append(GroupImpFact[i])
	BestOlap.append(olapNum[i])
        BestGroupParts.append(BetterParts[i])
        BestPartBounds.append(partBounds[i])
	BestPartDist.append(distN[i])
    
    #print BoundParts

    if plot == 1:
            count = 0
            for (x1,y1,x2,y2) in BestBounds:
              cv2.rectangle(im1, (int(x1*scaleFact), int(y1*scaleFact)), (int(x2*scaleFact), int(y2*scaleFact)), (0, 255, 0), 2)
              cv2.putText(im1, str(BestOlap[count]), (int(x1*scaleFact), int(y1*scaleFact)+count*15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 100)  
              cv2.putText(im1, str(BestConf[count]), (int(x1*scaleFact)+25, int(y1*scaleFact)+count*15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 100)  
              count=count+1
    
    if plot==1:
	    count = 0
            for (x1,y1,x2,y2) in BestBounds:
              cv2.rectangle(im1, (int(x1*scaleFact), int(y1*scaleFact)), (int(x2*scaleFact), int(y2*scaleFact)), (0, 255, 255), 2)
              cv2.putText(im1, str(BestConf[count]), (int(x1*scaleFact+25), int(y1*scaleFact+count*15)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 100)  
              count=count+1

            windowName="Object Proposals"
  	    #cv2.namedWindow(windowName, cv2.WINDOW_NORMAL)
  	    #cv2.resizeWindow(windowName, im1.shape[1]/2, im1.shape[0]/2)
            cv2.imshow(windowName, im1)


    if len(BestConf)>0:
      #MaxConfIndx = BestConf.index(min(BestConf))
      BestIndx=[i for (i,j) in zip(range(0, len(BestOlap)),BestOlap) if j==max(BestOlap)]
      ConfBestOlap=[BestConf[i] for i in BestIndx]
      MaxConfIndx = BestIndx[ConfBestOlap.index(min(ConfBestOlap))]
      pickedParts = BestGroupParts[MaxConfIndx]
      pick = [BestBounds[MaxConfIndx]]
      PickedPartDict, LEye, REye = map2part(pickedParts, BestPartBounds[MaxConfIndx], PartTag, scaleFact, imFidu, fiduFlag)
      #print LEye, REye
    else:
      pick = []
      PickedPartDict=[]    
      LEye = []
      REye = []

    PartDict = {}

    for i, bestParts in enumerate(BestGroupParts):
            PartDict[i]={}
            PartDict[i]['LEye']=[]
            PartDict[i]['REye']=[]
	    PartDict[i]['BBound']=BestBounds[i]
	    PartDict[i]['Dist']=BestPartDist[i]
            PartDict[i]['Parts'], PartDict[i]['LEye'], PartDict[i]['REye'] = map2part(bestParts, BestPartBounds[i], PartTag, scaleFact, imFidu, fiduFlag)
    
    return BestBounds, pick, PartDict, PickedPartDict, LEye, REye
    

