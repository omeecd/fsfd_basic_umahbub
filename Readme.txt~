======================================================================================
This package contains the code for Facial Segment Based Face Detector (FSFD) introduced 
in:

Mahbub, U.; Patel, V. M; Chandra, D.; Barbello, B.; Chellappa, R.; , 
"Partial Face Detection for Continuous Authentication", The International 
Conference on Image Processing (ICIP), 2016

A draft version of the paper can be found here: http://arxiv.org/pdf/1603.09364.pdf

Please cite the paper if you are using this code:

@author: Upal Mahbub 
	 Graduate Student, University of Maryland, College Park
	 umahbub@umd.edu
======================================================================================
Read the COPYING file for copyright information before using the code.

Check out the run.sh file to see instructions and explanations about the parameters
and running the code. The method is still beging studied and upgraded and this is a
priliminary version of the experiment. 

Some test images are provided from the UMDAA02-FD dataset in the TestImages folder to 
show how the code runs.

The svm and adaboost haar cascades were pre-trained and are provided with this package. 

You need to write your own code to evaluate the performance. After running this code
on the test data, a text file is created starting with the tag "Test" in the ./results
folder which contains the output. The format is as follows:

Location_of_the_Image	x1, y1, x2, y2		confidence_score	processing_time

(x1, y1) and (x2, y2) are the top-left and bottom-right corners of the face in the (rotated) 
image. If no face is detected then it will store "NoFace" instead of the coordinates and 
set the confidence score to 0. 

Considering 50% overlap with the ground truth an ROC can easily be drawn using the
confidence score to evaluate the performance of the detector.

Please contact Upal Mahbub (umahbub@umd.edu) for any issues on using this code.Also,
we will be glad to have your feedback about the performance of the code on your dataset.





