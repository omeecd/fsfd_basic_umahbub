
import pdb
import numpy as np
import cv2
import cv2.cv as cv
import sys
import os
import glob
import scipy as sc
import pickle
import sys
import argparse
from sklearn import metrics
import scipy.io
import pylab as pl
import random
import itertools
from sklearn import svm

########### Parse Arguments ###################################################
parser=argparse.ArgumentParser()
parser.add_argument("-Comb","--Combination", help="Combinationo of parts", default = '0')
parser.add_argument("-in2", "--input2", help="Detection Result of Methods")
parser.add_argument("-Olap","--OlapN", help="Number of segments in a proposal", default = '2', type=int)
parser.add_argument("-Th","--threshIN", help="Overlap Threshold (between 1 to 100)", default = '60', type=int)
parser.add_argument("-PP", "--PreProc", help="Enable Image Preprocessing", action="store_true", default="false")
parser.add_argument("-Tn","--TrSetNo", help="Training Set Number (1, 2, 3, ...)", type=int, default = 1)
parser.add_argument("-Tr","--TrainRatio", help="Percent train data (between 1 to 100)", type=int, default = 20)
parser.add_argument("-InFileList", "--ListOfTrainFiles", help="Text file containing path to the train images", default='./input/AATrain.txt')


args=parser.parse_args()
olaps=args.OlapN;
input2=args.input2
input2Pkl=input2.replace('.txt', '.pkl')
threshIN=args.threshIN*1.0/100
PreProc= args.PreProc
n=args.TrSetNo
TrainRatio=args.TrainRatio
combNo = args.Combination


if PreProc==1:
	ppFlag='_PP'
else:
	ppFlag=''

inputGT=args.ListOfTrainFiles
outputFileImp = './output%s_LB/FinalImp' %(ppFlag)+'_'+(input2.split('/')[-1].replace('.txt', '_%dpercent.pkl'%(threshIN*100)))
outputFileSVM = './output%s_LB/SVM' %(ppFlag)+'_'+(input2.split('/')[-1].replace('.txt', '_%dpercent.dat'%(threshIN*100)))
OutFolder = "./output_PP_LB/"

Comb={}

Comb['0']= {'Nose':1, 'EyePar':1, 'UL34':1, 'UR34':1, 'U12':1, 'L34':1, 'UL12':1, 'R12':1, 'L12':1, 'U34':1, 'UR12':1,  'B34':1, 'R34':1, 'B12':1, 'VJ':0,'VJProf':0}
Comb['5']= {'Nose':1, 'EyePar':1, 'UL34':1, 'UR34':1, 'U12':1, 'L34':1, 'UL12':1, 'R12':1, 'L12':1, 'U34':0, 'UR12':0,  'B34':0, 'R34':0, 'B12':0, 'VJ':0,'VJProf':0}

svm_params = dict( kernel_type = cv2.SVM_LINEAR, svm_type = cv2.SVM_C_SVC, C=2.67, gamma=5.383)

############################### Functions ########################################
def findsubsets(S,m): 
	return (itertools.combinations(S,m))

##################################################################################

AllPartNames = {'VJ':'Full', 'U12': 'U_1_2', 'U34':'U_3_4', 'Nose':'Nose', 'B12': 'B_1_2', 'UL34': 'UL_3_4', 'UR34': 'UR_3_4', 'L12': 'L_1_2', 'R12': 'R_1_2', 'UL12': 'UL_1_2', 'UR12': 'UR_1_2', 'EyePar': 'EyePair', 'VJProf': 'VJProf', 'L34': 'L_3_4', 'R34': 'R_3_4', 'B34': 'B_3_4'}


if __name__ == "__main__":
	PartNames = []
	for i in Comb[combNo]:
		if Comb[combNo][i]==1:
			PartNames.append(AllPartNames[i])

	PartNames=sorted(PartNames)
	print 'Here are the Parts: ', PartNames
	f1=open(inputGT, 'r')
	f2=open(input2, 'r')
	file1={}
	count = 0 
	for x in f1:	
		tag=x.split('\t')[0].replace('/', '_').replace('.jpg', '')[1:]
		temp=x.split('\t')
		coord = temp[1][:-1]
		if coord!='NoFace':
			coord=coord.split(',')
			val = [int(coord[0]), int(coord[1]), int(coord[2]), int(coord[3])]
		else:
			val = 'NoFace'
		file1[tag]= val
		count +=1
	
	file2={}
	count = 0
	
	for x in f2:
		temp=x[:-1].split('\t')
		tag=temp[0].replace('/', '_').replace('.jpg', '')[1:]
		val=temp[1].split('\n')[0]
		if len(temp)>2:		
			parts=temp[2:-1]
			pDist=float(temp[-1])
		else:
			parts=[]
			pDist=[]
		if val!='NoFace':
			val=val.split(',')
		if tag not in file2:
			file2[tag]=[]
		file2[tag].append([val, parts, pDist])
		count+=1

	N1=len(file1.keys())
	N2=len(file2.keys())
	PartPosVal={}
	PartPosProb={}
	PartNegVal={}
	PartNegProb={}
	for i in PartNames:
		PartPosVal[i]=0
		PartPosProb[i]=[]
		PartNegVal[i]=0
		PartNegProb[i]=[]

	count = 0
	CntFacePos=0
	CntFaceNeg=0
	for key in file2:
		if file2[key][0][0]!='NoFace' and file1[key]!='NoFace':
			count = count+1
			GTBox=file1[key]
			for i in file2[key]:		
				BBox = i[0]
				Parts=i[1]		
				SGT=(int(GTBox[2])-int(GTBox[0]))*(int(GTBox[3])-int(GTBox[1]))
				STest=(int(BBox[2])-int(BBox[0]))*(int(BBox[3])-int(BBox[1]))

				SI= max(0, min(int(GTBox[2]), int(BBox[2])) - max(int(GTBox[0]), int(BBox[0]))) * max(0, min(int(GTBox[3]), int(BBox[3])) - max(int(GTBox[1]), int(BBox[1])))
				SU = SGT + STest - SI
				Score=(SI*1.0)/SU
				if Score>=threshIN:
					for j in Parts:
						PartPosVal[j]+=1
						PartPosProb[j].append(Score)
					CntFacePos+=1
				else:
					for j in Parts:
						PartNegVal[j]+=1
						PartNegProb[j].append(1-Score)
					CntFaceNeg+=1


		elif file2[key][0][0]!='NoFace' and file1[key]=='NoFace':
			count = count+1
			for i in file2[key]:		
				BBox = i[0]
				Parts=i[1]		
				for p in PartNames:
					if p not in Parts:
						PartPosVal[p]+=1
					else:
						PartNegVal[p]+=1
				CntFaceNeg+=1
				CntFacePos+=1	


	Nparts=len(PartPosVal)	
	ImpFactPos={}
	ImpFactNeg={}
	ImpFactMeanPos={}
	ImpFactMeanNeg={}
	ImpFactOverallSum=[]
	ImpFactOverallProd=[]
	for parts in PartPosVal:
		ImpFactPos[parts]=PartPosVal[parts]*1.0/CntFacePos
		ImpFactNeg[parts]=PartNegVal[parts]*1.0/CntFaceNeg
		TotalImpSum=(ImpFactPos[parts]+(1-ImpFactNeg[parts]))/2
		TotalImpProd=(ImpFactPos[parts]*(1-ImpFactNeg[parts]))
		
		ImpFactOverallSum.append([TotalImpSum, parts])
		ImpFactOverallProd.append([TotalImpProd, parts])


	maxImp=0
	minImp=1.0
	for i in ImpFactOverallSum:
		if i[0]>=maxImp:
			maxImp=i[0] 	
		if i[0]<=minImp:
			minImp=i[0] 	

	maxImpProd=0
	minImpProd=1.0
	for i in ImpFactOverallProd:
		if i[0]>=maxImpProd:
			maxImpProd=i[0] 	
		if i[0]<=minImpProd:
			minImpProd=i[0] 


	RevisedImpFactSum={}		
	for i in ImpFactOverallSum:
		RevisedImpFactSum[i[1]] = i[0];
		print i[1], '%0.4f' %RevisedImpFactSum[i[1]] 
	print 

	RevisedImpFactProd={}	
	for i in ImpFactOverallProd:
		RevisedImpFactProd[i[1]] = i[0]
		print i[1], '%0.4f' %RevisedImpFactProd[i[1]] 


 	wrt=1
	print
	if wrt==1:
		O1=open('./output%s_LB/SortRes_%s' %(ppFlag, input2.split('/')[-1]), 'w')
		O1.write('ImpFactOverallSum\n')
	for i in sorted(ImpFactOverallSum, reverse=True):
		Txt= '%s \t %0.4f\n' % (i[1], i[0])
		if wrt==1:
			O1.write(Txt)

	O1.write('\nRevisedImpFactSum\n')
	for i in sorted(ImpFactOverallSum, reverse=True):
		Txt= '%s \t %0.4f\n' % (i[1], RevisedImpFactSum[i[1]])
		if wrt==1:
			O1.write(Txt)

	if wrt==1:	
		O1.write('\nImpFactOverallProd\n')
	
	for i in sorted(ImpFactOverallProd, reverse=True):
		Txt= '%s \t %0.4f\n' % (i[1], i[0])
		if wrt==1:
			O1.write(Txt)

	O1.write('\nRevisedImpFactProd\n')
	for i in sorted(ImpFactOverallProd, reverse=True):
		Txt= '%s \t %0.4f\n' % (i[1], RevisedImpFactProd[i[1]])
		if wrt==1:
			O1.write(Txt)

	if wrt==1:		
		O1.close()

	ImpFactList={}
	for key in file2:
		ImpFactList[key]=[]
		if file2[key][0][0]!='NoFace' and file1[key]!='NoFace':
			GTBox=file1[key]
			for i in file2[key]:
				if len(i[1])>=olaps:		
					BBox = i[0]
					whRatio=(int(BBox[3])-int(BBox[1]))*1.0/(int(BBox[2])-int(BBox[0]))
					Parts=i[1]
					PDist=i[2]		
					SGT=(int(GTBox[2])-int(GTBox[0]))*(int(GTBox[3])-int(GTBox[1]))
					STest=(int(BBox[2])-int(BBox[0]))*(int(BBox[3])-int(BBox[1]))

					SI= max(0, min(int(GTBox[2]), int(BBox[2])) - max(int(GTBox[0]), int(BBox[0]))) * max(0, min(int(GTBox[3]), int(BBox[3])) - max(int(GTBox[1]), int(BBox[1])))
					SU = SGT + STest - SI
					Score=(SI*1.0)/SU
					if Score>=threshIN:
						ImpFactSum=0
						ImpFactProd=0
						for j in Parts:
							ImpFactSum+=RevisedImpFactSum[j]
							ImpFactProd+=RevisedImpFactProd[j]
						ImpFactList[key].append([Parts, ImpFactSum, ImpFactProd, Score, PDist, whRatio, STest, 1])
					else:
						ImpFactSum=0
						ImpFactProd=0
						for j in Parts:
							ImpFactSum+=RevisedImpFactSum[j]
							ImpFactProd+=RevisedImpFactProd[j]
						ImpFactList[key].append([Parts, ImpFactSum, ImpFactProd, Score, PDist, whRatio, STest, 0])

		if file2[key][0][0]!='NoFace' and file1[key]=='NoFace':
			ImpFactSum=0
			ImpFactProd=0		
			for i in file2[key]:
				BBox = i[0]
				whRatio=(int(BBox[3])-int(BBox[1]))*1.0/(int(BBox[2])-int(BBox[0]))
				STest=(int(BBox[2])-int(BBox[0]))*(int(BBox[3])-int(BBox[1]))
				PDist=i[2]
				Parts=i[1]
				if len(i[1])>=olaps:	
					for j in Parts:
						ImpFactSum+=RevisedImpFactSum[j]
						ImpFactProd+=RevisedImpFactProd[j]
					ImpFactList[key].append([Parts, ImpFactSum, ImpFactProd, 0,  PDist, whRatio, STest, 0])
	
	PartList=[]
	IFactSum=[]
	IFactProd=[]
	OlapScore=[]
	Distance=[]
	WHRatio=[]
	Area=[]
	Match=[]
	for k in ImpFactList:
		for j in ImpFactList[k]:
			PartList.append(sorted(j[0]))
			IFactSum.append(j[1])
			IFactProd.append(j[2])
			OlapScore.append(j[3])
			Distance.append(j[4])
			WHRatio.append(j[5])
			Area.append(j[6])
			Match.append(j[-1])
	MatchIndx=[i for (i,j) in zip(range(0, len(Match)), Match) if j == 1]
	NonMatchIndx=[i for (i,j) in zip(range(0, len(Match)), Match) if j == 0]
	#UniquePartSet=[list(t) for t in set(map(tuple, PartList))]

	temp=set()
	TotParts=[k for k in RevisedImpFactSum]
	for j in range(len(RevisedImpFactSum), 1, -1):
		temp.update(k for k in findsubsets(TotParts, j))
	UniquePartSet=[sorted(list(t)) for t in temp]

	PartListFace=[PartList[i] for i in MatchIndx]
	PartListNonFace=[PartList[i] for i in NonMatchIndx]

	zF=map(tuple, PartListFace)
	zNF=map(tuple, PartListNonFace)
	UniquePartProb=[]
	posProb=0
	negProb=0
	posPartProb=0
	negPartProb=0
	for i in UniquePartSet:
		partImp=[]
		if i in PartList:
			indx=PartList.index(i)
			indices = [m for m, x in enumerate(PartList) if x == i]
			posProb= zF.count(tuple(i))*1.0/len(zF)
			negProb=(1.0-zNF.count(tuple(i))*1.0/len(zNF))
			
			for kk in i:
				posPartProb+=ImpFactPos[kk]
				negPartProb+=1-ImpFactNeg[kk]
			posPartProb=posPartProb/len(i)
			negPartProb=negPartProb/len(i)
			OlSc=0
			for j in indices:
				OlSc+=OlapScore[j];
			OlSc=OlSc*1.0/len(indices)
		

			

		else:		
			posProb=1.0/(len(zF))
			negProb=1.0/(len(zNF))
			posPartProb=0.0000000001
			negPartProb=0.0000000001
			OlSc=0.0000000001

		for kk in PartNames:
			if kk in i:
				partImp.append(ImpFactPos[kk])
				partImp.append(1-ImpFactNeg[kk])
			else:
				partImp.append(0)
				partImp.append(0)

		ft=([posProb, negProb, posPartProb, negPartProb, OlSc])
		for jj in partImp:
			ft.append(jj)
		UniquePartProb.append(ft)


	############ Train SVM ##############################
	print 'Now creating feature vector ...'

	XPos=[]
	YPos=[]
	XNeg=[]
	YNeg=[]
	for i in MatchIndx:
		indx=UniquePartSet.index(sorted(PartList[i]))
		features=[UniquePartProb[indx][0], UniquePartProb[indx][1]] #, Distance[i], Area[i]]
		features.extend(UniquePartProb[indx][5:]) #, Distance[i], Area[i]]
		print features
		XPos.append(features);
		YPos.append(1)
		
	for i in NonMatchIndx:
		indx=UniquePartSet.index(sorted(PartList[i]))
		features=[UniquePartProb[indx][0], UniquePartProb[indx][1]] #, Distance[i], Area[i]]
		features.extend(UniquePartProb[indx][5:]) #, Distance[i], Area[i]]
		print features
		XNeg.append(features);
		YNeg.append(0)

        Xall=XPos+XNeg
        Yall=YPos+YNeg

	FinalImp={}
	FinalImp['RevisedSum'] = RevisedImpFactSum
	FinalImp['RevisedProd']= RevisedImpFactProd
	FinalImp['UniquePartSet']= UniquePartSet
	FinalImp['UniquePartProb']= UniquePartProb


	O23=open(outputFileImp, 'wb')
	pickle.dump(FinalImp, O23)
	O23.close()
	
	print 'Training SVM ...'

	SVM = svm.LinearSVC() #(probabiity='True', random_state=0)
	#SVM = svm.SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0, degree=3, gamma=0.0, kernel='rbf', max_iter=-1, probability=True, random_state=None, shrinking=True, tol=0.001, verbose=False)
	SVM.fit(Xall, Yall) 
	print 'Saveing Data to ', outputFileSVM
	O23=open(outputFileSVM, 'wb')
	pickle.dump(SVM, O23)
	O23.close()
	
	
