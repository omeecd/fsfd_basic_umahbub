============================================================================================
This package contains the code for Facial Segment Based Face Detector (FSFD) introduced in:

U. Mahbub, V. M. Patel, D. Chandra, B. Barbello and R. Chellappa, "Partial face detection for continuous authentication," 2016 IEEE International Conference on Image Processing (ICIP), Phoenix, AZ, USA, 2016, pp. 2991-2995.
doi: 10.1109/ICIP.2016.7532908

A draft version of the paper can be found here: http://arxiv.org/pdf/1603.09364.pdf

Please cite the paper if you are using this code. The bibtex is as follwos:
@INPROCEEDINGS{FSFD_UMahbub,
author={U. Mahbub and V. M. Patel and D. Chandra and B. Barbello and R. Chellappa},
booktitle={2016 IEEE International Conference on Image Processing (ICIP)},
title={Partial face detection for continuous authentication},
year={2016},
pages={2991-2995},
keywords={Authentication;Detectors;Face;Face detection;Image segmentation;Proposals;Support vector machines;Active authentication;facial segments;partial face detection;smartphone front-camera images processing},
doi={10.1109/ICIP.2016.7532908},
month={Sept}}


@author: Upal Mahbub 
	 Graduate Student, University of Maryland, College Park
	 umahbub@umd.edu
============================================================================================
Read the COPYING file for copyright information before using the code.

This code for FSFD is expected to perform best to detect single faces from the front-camera
images of cell-phones.

Check out the run.sh file to see instructions and explanations about the parameters
and running the code. The method is still beging studied and upgraded and this is a
priliminary version of the experiment. 

Some test images are provided from the UMDAA02-FD dataset in the TestImages folder to 
show how the code runs.

The svm and adaboost haar cascades were pre-trained and are provided with this package. 

You need to write your own code to evaluate the performance. After running this code
on the test data, a text file is created starting with the tag "Test" in the ./results
folder which contains the output. The format is as follows:

Location_of_the_Image	x1, y1, x2, y2		confidence_score	processing_time

(x1, y1) and (x2, y2) are the top-left and bottom-right corners of the face in the (rotated) 
image. If no face is detected then it will store "NoFace" instead of the coordinates and 
set the confidence score to 0. 

Considering 50% overlap with the ground truth an ROC can easily be drawn using the
confidence score to evaluate the performance of the detector.

Please contact Upal Mahbub (umahbub@umd.edu) for any issues on using this code.Also,
we will be glad to have your feedback about the performance of the code on your dataset.





