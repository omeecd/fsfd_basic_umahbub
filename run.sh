#!/bin/bash


# ==============================================================
# run the in terminal: $ bash run_SVM.sh 
# @author: Upal Mahbub, University of Maryland, College Park
#	 umahbub@umd.edu
# ==============================================================

## Parameters 
## Note: Keep the parameters as it is in order to use the trained models
Scale="4" 		## scale factor: image resized to 1/scale size.
ThreshLearn="50"  	## overlap threshold when training
ThreshTest="50" 	## overlap threshold when testing 
Olap="2" 		## minimum number of overlaps required to denote a face
PP=1 			## enable image preprocessing
p="" 			## use "-p" to enable multiprocessing
stype='ValueSum' 	## Ignore
ImgSave=1 		## for saving images in the result folder

folder="Final"		## For the trained model files - ignore
TrainRatio=20		## For the trained model files - ignore
IN=20			## For the trained model files - ignore


TrDataSet="NameOfYourTrainingDataset"		## Give a name to your test dataset - this is just a tag
InFileListTr="./input/TrainImageList.txt" 	## List of location of the test images - check out the ./input/AANotDetected.txt file for format
RotateValTr=-90 				## Angle of rotation for the images before preprocessing. If the images are already in correct 


TeDataSet="NameOfYourTestDataset"		## Give a name to your test dataset - this is just a tag
InFileListTe="./input/TestImageList.txt" 	## List of location of the test images - check out the ./input/AANotDetected.txt file for format
RotateValTe=-90 				## Angle of rotation for the images before preprocessing. If the images are already in correct rotation, set this to zero.

for n in 1 #50
do
	for Comb in  5
	do
		echo "Processing Comb$Comb..."
		if [[ "$PP" -eq "1" ]]
		then
			PropFile="./models_PP_LB/Train"$folder""$n"_"$TrainRatio"_Proposed_"$TrDataSet"_PFD_Comb"$Comb"_Scale"$Scale"_IN"$IN"_PP.txt"
			ResFileTe="./result/TestPP"$folder""$n"_"$TrainRatio"_"$TeDataSet"_"$Comb"_Scale"$Scale"_"$stype"_IN"$IN"_PP.txt"
			PreProc="-PP"
			echo $PropFile
			echo "PreProcessing Turned On"
		else
			PropFile="./models_LB/Train"$folder""$n"_"$TrainRatio"_Proposed_"$TrDataSet"_PFD_Comb"$Comb"_Scale"$Scale"_IN"$IN".txt"
			ResFileTe="./result/Test"$folder""$n"_"$TrainRatio"_"$TeDataSet"_"$Comb"_Scale"$Scale"_"$stype"_IN"$IN".txt"
			PreProc=""
			echo $PropFile
			echo "No PreProcessing"
		fi


		if [[ "$ImgSave" -eq "1" ]]
		then 
			ImgSv="-ImgSv"
			echo "Image Saving Turned on"
		else
			ImgSv=""
			echo "Image Saving Turned Off"
		fi
		#echo 'Now Training ...'
		#python umd_PartialFD_"$folder"_Train_SVM.py -Sc $Scale -Comb $Comb $PreProc $p -Tn $n -Tr $TrainRatio -INTr $IN $ImgSv -InFileList $InFileListTr -RotateVal $RotateValTr #>> ./log/Train_log"$Comb".txt
		#echo 'Learning Priority List ... '
		#python LearnParamForParts_rev0_SVM.py -in2 $PropFile -Olap $Olap -Th $ThreshLearn $PreProc -Tn $n -Tr $TrainRatio -Comb $Comb -InFileList $InFileListTr  #>> ./log/Learn_log"$Comb".txt
		#echo 'Training Completed'
	
		echo 'Now Testing ...'
		python umd_PartialFD_"$folder"_Test_SVM.py -Sc $Scale -Comb $Comb $PreProc -Th $ThreshLearn $p -Tn $n -Tr $TrainRatio $ImgSv -INTe $IN -InFileList $InFileListTe -RotateVal $RotateValTe -resLoc $ResFileTe #>> ./log/Test_log"$Comb".txt
		echo 'Test Completed!!'
	done
done

